/* Not a leap year */
const seconds = 60 * 60;
const hours = 24;
const days = 365;
const secondsInYear = seconds * hours * days;
console.log('Seconds in a none leap year ' + secondsInYear);