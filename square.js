const process = require('process');
const side = process.argv.slice(2)[0] || 5;
const area = side**2;
console.log(`A square with the side ${side} has an area of ${area}`);