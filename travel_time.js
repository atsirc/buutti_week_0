const args = require('process');
const distance = Number(args.argv[2]) || 100;
const speed = Number(args.argv[3]) || 30;
const time = distance/speed;
/* Make time more easily readable */
const hours = Math.floor(time); 
const min = Math.floor(60*(time-hours));
console.log(`Travel time: ${hours} hours ${min} minutes`)