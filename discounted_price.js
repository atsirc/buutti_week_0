const process = require('process');
let price = 200;
let discount = 20;
if (process.argv.length > 3) {
    price = Number(process.argv[2]);
    discount = Number(process.argv[3]);
}
if (discount > 100) {
    console.log(`If the discount was ${discount}%, you would be spending more instead of less`);
} else {
    const discounted_price = Math.round(price*(1-0.01*discount));
    console.log('Price after discoutnt: ' + discounted_price);
}